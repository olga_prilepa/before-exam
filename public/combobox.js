function debounce(func, time) {
  let iTimeout = null
  return (...args) => {
    if(iTimeout) {
      window.clearTimeout(iTimeout)
    }
    iTimeout = window.setTimeout(() => {
      func(...args)
      iTimeout = null
    }, time)
  }
}

class ComboBox {
  constructor(inputName, message, variantsList) {
    this.inputName = inputName;
    this.message = message;
    this.variantsList = variantsList;
  }

  render () {
    //console.log(this.variantsList);
  };
}
export default ComboBox;
/* Реализуйте ComboBox и экспортируйте его при помощи export {ComboBox}*/