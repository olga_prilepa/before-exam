/*
* В файле combobox.js реализуйте ComboBox
* а сюда импортируйте его, например так import {ComboBox} from './combobox.js'
*/

import ComboBox from "./ComboBox.js";

const city = new ComboBox('city', 'Введите или выберите из списка', getCityList());
document.getElementById('row_city').appendChild(city.render());

const ide = new ComboBox('ide', 'Введите или выберите из списка', getIdeList());
document.getElementById('row_ide').appendChild(ide.render());

/* Реализуйте функцию поиска городов */
function getCityList() {
    // fetch('/city?name_like=Нов')
    return fetch(`/city`)
        .then(function(response) {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(`Запрос завершился не успешно: ${response.status} ${response.statusText}`);
            }
        })
        .then(function(city) {
            return Promise.resolve(city);
        })
        .catch(function (error) {
            return Promise.reject(error);
        });
}

/* Реализуйте функцию поиска IDE */
function getIdeList() {
    fetch('/ide')
}


/* Добавьте ComboBox для строки "Город"
* например так:
* const city = new ComboBox('city', 'Введите или выберите из списка', getCityList)
* document.getElementById('row_city').appendChild(city.render())
* Ваша реализация может сильно отличаться
* */

/* Добавьте ComboBox для строки "Предпочитаемая IDE"*/